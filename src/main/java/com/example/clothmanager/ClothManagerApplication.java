package com.example.clothmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClothManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClothManagerApplication.class, args);
    }

}
