package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClothManagementModel {
    private String size;
    private String season;
    private String gender;
    private String age;
    private String sale;

}
