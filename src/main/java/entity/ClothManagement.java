package entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class ClothManagement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //고유식별번호

    @Column(nullable = false, length = 20)
    private  String size; //옷 사이즈

    @Column(nullable = false, length = 20)
    private String season;// 계절별 옷

    @Column(nullable = false, length = 20)
    private String gender;// 성별 옷

    @Column(nullable = false, length = 20)
    private String age;// 연령별 옷

    @Column(nullable = false, length =20)
    private String sale;// 세일상품 별 옷



}
