package service;

import entity.ClothManagement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import repository.ClothManagementRepository;

@Service
@RequiredArgsConstructor
public class ClothManagementService {
    private final ClothManagementRepository clothManagementRepository;
    public void setClothManagement(String size, String season, String gender, String age, String sale) {
        ClothManagement addData = new ClothManagement();
        addData.setSize(size);
        addData.setSeason(season);
        addData.setGender(gender);
        addData.setAge(age);
        addData.setSale(sale);
        clothManagementRepository.save(addData);
    }

}
