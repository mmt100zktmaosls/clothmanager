package repository;

import entity.ClothManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClothManagementRepository extends JpaRepository<ClothManagement, Long> {

}
