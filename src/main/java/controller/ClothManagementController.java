package controller;

import lombok.RequiredArgsConstructor;
import model.ClothManagementModel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.ClothManagementService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cloth-management")
public class ClothManagementController {
    private final ClothManagementService clothManagementService;

    @PostMapping("/data")
    public String setClothManagement(@RequestBody ClothManagementModel managementModel) {
        clothManagementService.setClothManagement(managementModel.getSize(), managementModel.getSeason(), managementModel.getGender(), managementModel.getAge(), managementModel.getSale());
        return "OK";
    }
}
